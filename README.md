# EBookcolletction
![](https://github.com/WeihuaGu/EBookcollection/blob/master/book.jpg)
上传自己收集的电子书吧！
##设立项目的原因
总是苦于找不到某些书，所以创建了这个项目用来收集电子书。

Always could not find some books, so the creation of this project to collect books.

##欢迎fork并上传自己收集的电子书

###文件夹
每个文件夹代表着分类。
文件夹的命名是中文(英文),比如‘法律(Law)'
###上传书籍
如果上传了书籍，请将书籍更新添加到index.txt书籍索引中！

如果是中文电子书,填写中文路径，如民法思维.epub添加

法律/民法/民法总论.epub

如果是英文电子书，填写英文路径，如get things down.epub

life/efficiency/get things down.epub

##只是收集电子书？不是，你还可以写书评的。

点击本项目的wiki，直接书写……
###[书语](https://github.com/WeihuaGu/EBookcollection/wiki/%E4%B9%A6%E8%AF%AD)
